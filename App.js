import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'

import {
  HomeScreen,
  DirectMessageListScreen,
  SearchScreen,
  LikesScreen,
  MyProfileScreen
} from 'screens'
import { ITabBar, IIcon } from 'components'
import { Theme } from 'theme-consts'

const TabsHome = createBottomTabNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <IIcon name="home_icon" style={{ color: tintColor, fontSize: 32 }} />
        ),
      },
    },
    SearchScreen: {
      screen: SearchScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <IIcon name="search_icon" style={{ color: tintColor, fontSize: 32 }} />
        ),
      },
    },
    LikesScreen: {
      screen: LikesScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <IIcon name="like_icon" style={{ color: tintColor, fontSize: 32 }} />
        ),
      },
    },
    MyProfileScreen: {
      screen: MyProfileScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <IIcon name="profile_icon" style={{ color: tintColor, fontSize: 32 }} />
        ),
      },
    },
  },
  {
    tabBarComponent: ITabBar,
    tabBarOptions: {
      activeTintColor: Theme.COLORS.$darkGrey,
      inactiveTintColor: Theme.COLORS.$mediumGrey,
    },
    lazy: true,
    swipeEnabled: false,
    animationEnabled: false,
  },
)

const AppNavigator = createStackNavigator(
  {
    Tabs: {
      screen: TabsHome,
    },
    DirectMessageList: {
      screen: DirectMessageListScreen,
    },
  },
  { headerMode: 'none' },
)

export default createAppContainer(AppNavigator)
